enscribe (0.1.0-5) unstable; urgency=medium

  * QA upload.

  * Updated vcs in d/control to Salsa.
  * Added d/gbp.conf to enforce the use of pristine-tar.
  * Updated Standards-Version from 4.6.2 to 4.7.0.
  * Replaced obsolete pkg-config build dependency with pkgconf.
  * Corrected lintian override to match current output.

 -- Petter Reinholdtsen <pere@debian.org>  Fri, 26 Apr 2024 21:30:12 +0200

enscribe (0.1.0-4) unstable; urgency=medium

  * QA upload.
  * Using new DH level format. Consequently:
    - debian/compat: Removed.
    - debian/control: Changed from 'debhelper to 'debhelper-compat' in
      Builds-Depends field and bumped level to 13.
  * debian/changelog: Removed trailing whitespaces.
  * debian/control:
    - Added Rules-Requires-Root field.
    - Bumped Standards-Version to "4.6.2".
    - Changed Priority from extra to optional.
  * debian/copyright: Updated packaging copyright information.

 -- Valdecir Sarat <valdecir.sarat@gmail.com>  Sat, 02 Dec 2023 16:25:40 -0300

enscribe (0.1.0-3) unstable; urgency=medium

  * QA upload
  * Dropped alternative build-dependency on libgd2-xpm-dev (closes: #881746)

 -- Ralf Treinen <treinen@debian.org>  Tue, 14 Nov 2017 21:05:08 +0100

enscribe (0.1.0-2) unstable; urgency=medium

  * QA upload
  * Set maintainer field to Debian QA Group (see #839652)
  * Packaging cleanup. Closes: #669621, #817448
    - Re-write debian/copyright in format 1.0
    - Declare compliance with policy 3.9.8
    - Convert to source format 3.0 (quilt)
    - Bump debhelper compat level to 10
    - Use dh7-style debian/rules
    - Enable full hardening build
    - Fix typos
  * Reorder libraries to fix FTBFS with ld --as-needed. Closes: #638843,
    LP: #770933

 -- Christoph Biedl <debian.axhn@manchmal.in-ulm.de>  Sun, 25 Dec 2016 12:58:52 +0100

enscribe (0.1.0-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "FTBFS in squeeze: Nonexistent build-dependency: 'libgd-dev'":
    switch order of libgd-dev | libgd2-xpm-dev to put the real package before
    the virtual one (closes: #595836).

 -- gregor herrmann <gregoa@debian.org>  Sun, 19 Sep 2010 15:48:09 +0200

enscribe (0.1.0-1) unstable; urgency=low

  * New upstream version
    - Adds audio mask feature.
    - iFFT blocksize can go to 7 (65536)
  * repack source to remove upstream binary
  * added pathes/02-FFTblocksizenorm - normalizes the two FFT blocksize
    arguments (upstream set one to be able to go to 7, but not the
    other, the are the same now).

 -- Nick Rusnov <nickrusnov@debian.org>  Mon, 10 Aug 2009 20:34:16 -0700

enscribe (0.0.4-3) unstable; urgency=low

  * Ack NMU.
  * Bump standards version to 3.7.2.0.
  * Fix control to match override (sound-extra).
 -- Nick Rusnov <nickrusnov@debian.org>  Tue, 27 Feb 2007 14:32:30 -0800

enscribe (0.0.4-2.2) unstable; urgency=low

  * Non-maintainer upload.
  * Build-depend on libgd2-noxpm-dev (Closes: #347916).

 -- Luk Claes <luk@debian.org>  Fri, 13 Jan 2006 23:24:27 +0100

enscribe (0.0.4-2.1) unstable; urgency=high

  * Non-maintainer upload.
  * Build-depend on libgd2-xpm-dev (Closes: #343912).

 -- Luk Claes <luk@debian.org>  Tue, 27 Dec 2005 19:33:44 +0100

enscribe (0.0.4-2) unstable; urgency=low

  * Durr. Adding docbook-to-man and dpatch to dependencies. Sorry for the
    sloppy. (Closes: #303472)

 -- Nick Rusnov <nickrusnov@debian.org>  Wed,  6 Apr 2005 15:00:22 -0700

enscribe (0.0.4-1) unstable; urgency=low

  * Initial Release. (Closes: #273256)

 -- Nick Rusnov <nickrusnov@debian.org>  Mon,  1 Nov 2004 14:52:24 -0800
